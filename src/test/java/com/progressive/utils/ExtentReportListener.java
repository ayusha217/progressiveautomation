package com.progressive.utils;

import java.time.LocalDateTime;
import java.util.List;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.progressive.utils.reporting.DBManager;
import com.progressive.utils.reporting.TestStatus;

public class ExtentReportListener implements ISuiteListener, ITestListener, IReporter {

	ExtentReports extent;
	ExtentSparkReporter spark;
	private TestStatus testStatus;

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestStart(ITestResult result) {
		this.testStatus = new TestStatus();

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		extent.createTest(result.getMethod().getMethodName()).log(Status.PASS,
				result.getMethod().getMethodName() + " is PASSED");
		org.testng.Reporter.log("Test Case PASSED " + result.getMethod().getMethodName(), true);
		this.sendStatus(result, "PASS");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		extent.createTest(result.getMethod().getMethodName()).log(Status.FAIL,
				result.getMethod().getMethodName() + " is Failed");

		// call method to capture result to db
		org.testng.Reporter.log("Test Case Failed " + result.getMethod().getMethodName(), true);
		this.sendStatus(result, "FAILED");

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		extent.createTest(result.getMethod().getMethodName()).log(Status.SKIP,
				result.getMethod().getMethodName() + " is Skipped");
		org.testng.Reporter.log("Test Case Skipped " + result.getMethod().getMethodName(), true);
		this.sendStatus(result, "SKIPPED");
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ISuite suite) {

		extent = new ExtentReports();

		spark = new ExtentSparkReporter("ExtentReport.html");

		extent.attachReporter(spark);

	}

	@Override
	public void onFinish(ISuite suite) {
		extent.flush();

	}

	private void sendStatus(ITestResult iTestResult, String status) {
		System.out.println("sendStatus() initiated...");

		this.testStatus.setTestClass(iTestResult.getTestClass().getName());
		this.testStatus.setDescription(iTestResult.getMethod().getDescription());
		this.testStatus.setStatus(status);
		this.testStatus.setExecutionDate(LocalDateTime.now().toString());
		if (status != "PASS") {
			this.testStatus.setLog(iTestResult.getThrowable().getMessage());
		} else {
			this.testStatus.setLog(iTestResult.getMethod().getMethodName() + " testcase is PASS");
		}
		this.testStatus.setTestCase(iTestResult.getMethod().getMethodName());
		
		DBManager.send(this.testStatus);
		System.out.println("sendStatus() completed..!");
	}

}
