package com.progressive.testcases.api;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;



import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_GET_Test {
	
	@Test
	public void GET_Sample_Test() throws ParseException {
		
		
		//Enviornment end point
		RestAssured.baseURI = "https://reqres.in";
		
		//API Path/URL
		String users_URI = "/api/users?page=2";
		
		
		RequestSpecification request = RestAssured.given();
		
		//Request headers
		request.header("Content-Type","application/json");
		
		
		//Send api request
		Response response = request.get(users_URI);
							
		
		System.out.println("status code :"+response.getStatusCode());
		JSONParser parse = new JSONParser(); 
		JSONObject obj = (JSONObject) parse.parse(response.asString());
		
		 String result = obj.get("total_pages").toString();
		System.out.println("The result is:"+ result);
		
		 String result1 = obj.get("total").toString();
			System.out.println("The result is:"+ result1);
			
			JSONArray result2 = (JSONArray) obj.get("data");
			for(int i=0;i<result2.size();i++) {
				JSONObject obj1 = (JSONObject) result2.get(i);
				System.out.println(obj1.get("id").toString());
				System.out.println(obj1.get("email").toString());
				System.out.println(obj1.get("first_name").toString());
				System.out.println(obj1.get("last_name").toString());
				
				
				
			}
					
			
		/*
		 * 1. Status codes
		 * 2. API Response Content[JSON Assertions]
		 * 
		 * 2XX - Series  -  API Sucess 
		 * 200 - Success - GET
		 * 201 - Created sucessfully, POST and PUT
		 * 
		 * 4xx -  Error in your api execution
		 * 401 - Un authoreized
		 * 400 - Bad Request
		 * 403 - Access Denined
		 * 
		 * 5xx - 500 Series status codes
		 * 500 - Internal Server Erroor
		 * 503-  Forbidden error
		 * this represents server side errors 
		 * 
		 */
	}

}
