package com.progressive.testcases.mousehover;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.progressive.testcases.BaseTest;

public class MousehoverDemo extends BaseTest{
	
	@BeforeMethod
	public void beforeMethod() {
		launchBrowser("Chrome");
		driver.navigate().to("https://www.irctctourism.com/");
	}
	
	@AfterMethod
	public void afterMethod() {
		//driver.quit();
	}
	
	@Test
	public void mouseHoverTest() throws InterruptedException {
		
		WebElement flights = driver.findElement(By.xpath("//span[contains(text(),'Flights')]"));
		
		//mouse hover operation
		Actions act = new Actions(driver);
		
		act.moveToElement(flights).build().perform();
		Thread.sleep(3000);
		
		flights.click();
		
		
		WebElement deluxTrains = driver.findElement(By.xpath("//span[contains(text(),'Deluxe Train')]"));
		act.moveToElement(deluxTrains).build().perform();	
		
		
		
	}

}
