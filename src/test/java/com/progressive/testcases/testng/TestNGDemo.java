package com.progressive.testcases.testng;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.progressive.testcases.BaseTest;

public class TestNGDemo extends BaseTest{
	
	
	
	@BeforeMethod
	public void beforeMethod() {
		launchBrowser("Chrome");
		driver.navigate().to("https://www.linkedin.com/");
	}
	
	@AfterMethod
	public void afterMethod() {
		//driver.quit();
	}
	
	
	@Test(description = "LinkedIn login Test")
	@Parameters({"username","password"})
	public void linkedInLoginTest( String username, @Optional("Admin@12345")String password) throws InterruptedException {
		
		System.out.println("Linkedin login Test");
		driver.findElement(By.linkText("Sign in")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(username);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(password);
		
	}

}
