package com.progressive.utils.reporting;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestStatus {

	@JsonProperty("testClass")
	private String testClass;

	@JsonProperty("description")
	private String description;

	@JsonProperty("status")
	private String status;

	@JsonProperty("executionTime")
	private String executionTime;

	@JsonProperty("log")
	private String log;

	@JsonProperty("testcase")
	private String testcase;

	/*
	 * @JsonProperty("testcase") private String newFiled;
	 */

	public void setTestClass(String testClass) {
		this.testClass = testClass;
	}

	public void setDescription(String description) {
		this.description = description;

	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setExecutionDate(String executionTime) {
		this.executionTime = executionTime;
	}

	public void setLog(String log) {
		System.out.println("log from Teststatus class is : " + log);
		this.log = log;
	}

	public void setTestCase(String testcase) {
		this.testcase = testcase;
	}

}
